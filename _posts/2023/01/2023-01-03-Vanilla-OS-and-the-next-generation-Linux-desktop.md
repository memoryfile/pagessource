---
title: Vanilla OS and the next-generation Linux desktop
date: 2023-1-03 12:04:00 +/-TTTT
categories: [Linux, Immutable]
tags: [flatpak, Flathub, AUR, Distrobox, Fedora Silverblue]
---

[Vanilla OS](https://vanillaos.org/), an Ubuntu-based Linux distribution, recently had its first stable [release](https://vanillaos.org/2022/12/29/vanilla-os-22-10-kinetic.html). Much like other Ubuntu-based distributions, it emphasizes ease of use. However, it approaches this differently than many others.

This article will not discuss every aspect of Vanilla OS, but more so the technologies and workflow it employs. In my view, these could shape many popular Linux distributions in the future.

## Overview of Vanilla OS

Vanilla OS ships with a minimal, unmodified [GNOME](https://www.gnome.org/) desktop environment. By following GNOME upstream closely, it aims for a bug-free experience.

More importantly, Vanilla OS ships a workflow quietly forged by proponents of [image-based](https://blog.verbum.org/2020/08/22/immutable-%E2%86%92-reprovisionable-anti-hysteresis/) (or *immutable*) Linux desktops.

* Out of the box, it provides hassle-free updates and rollbacks. It offers this through immutability and auto-update functionality.
* It ships [Flatpak](https://flatpak.org/) and the largest Flatpak software repository, [Flathub](https://beta.flathub.org/).
* [`apx`](https://documentation.vanillaos.org/docs/apx/), a [Distrobox-based](https://github.com/89luca89/distrobox) package manager, is configured by default. It gives access to packages from Ubuntu, Fedora, Arch Linux, and more. It can also [integrate](https://vanillaos.org/2022/12/29/vanilla-os-22-10-kinetic.html) installed packages from these distributions into Vanilla OS.

![Distrobox subsystem](/assets/img/2023/01/distrobox_subsystem.png){: w="700" h="400" }

We can call this workflow — Flatpak, immutability, Distrobox — the *immutable workflow*. Combining everything together, Vanilla OS is unprecedentedly low-maintenance and powerful. It has the potential to become a staple for non-technical individuals and Linux veterans alike.

## What are these technologies?

### Flatpak

Flatpak is a containerized way to use applications on the Linux desktop. As detailed in my article [Distribution packaging for Linux desktop applications is unsustainable](https://memoryfile.codeberg.page/posts/Distribution-packaging-for-Linux-desktop-applications-is-unsustainable), Flatpak's model improves privacy, security, and ease of use on the Linux desktop.

Moreover, Flatpak provides a line of separation between one's applications and the base system. No matter the distribution they're on, Flatpak applications won't harm the base system.

### Immutability

Immutable Linux distributions take advantage of this line of separation by setting most core system files as read-only. This means that applications cannot easily corrupt one's base system.

This can work very well. Immutability is one reason why SteamOS on the [Steam Deck](https://store.steampowered.com/steamdeck) provides such a good experience to non-technical individuals. It prevents bricking issues resulting from dependency conflicts.

There's other significant material benefits that can be provided by immutability. Immutability looks different across various Linux distributions, but I'll discuss its most popular forms and benefits here.

#### Robust updates

Immutability enables more robust updates. For instance, take [`rpm-ostree`](https://coreos.github.io/rpm-ostree/) in [Fedora Silverblue](https://silverblue.fedoraproject.org/). Even if one's computer shuts down during an update, there's no harm. This is part of Silverblue's [atomic update](https://youtu.be/-hpV5l-gJnQ?t=51) model. Updates only go through if they're completed and successful.

Vanilla OS's immutability system, [ABRoot](https://documentation.vanillaos.org/), also provides this benefit by transacting between two root partitions.

#### Better rollbacks

Immutability also enables better rollback capabilities. This means that if something goes wrong on your system, a reinstall is often not necessary.

Once again, take Fedora Silverblue's immutability. One can go from Fedora Silverblue 37 to 36, for example, without experiencing irreconcilable breakage from dependency issues. This is because one can *pin* known working system states to the boot menu. If the rollback goes wrong, one can boot back into a known good state.

[ABRoot](https://github.com/Vanilla-OS/ABRoot) also lets one rollback to the *last* system version. System snapshots may also come to Vanilla OS in the future.

### Immutability's drawbacks and solutions

As aforementioned, Vanilla OS is an immutable distribution. Many people have tried immutable distributions in the past few years. Some have brought up a couple pain points:

#### Tinkering

> Immutability can seem to prevent the system tinkering that many Linux enthusiasts are fond of.

This depends on how the immutability is implemented. Distributions like [EndlessOS](https://endlessos.com/) certainly have less tinkering potential than Debian.

However, there's a lot of immutability implementations which *support* tinkering. Combined with rollbacks, it can be an even better experience than tinkering with traditional Linux distributions. It just works differently compared to them.

* [`rpm-ostree`](https://github.com/ostreedev/ostree) lets one compose custom [system configurations](https://github.com/castrojo/awesome-immutable#configs-from-the-community) and rebase to them. One also has the ability to overlay packages into the base system and to restore a 'clean' base system if necessary.
* [NixOS](https://nixos.org/) has always been a very friendly ground for technical users and tinkerers.
* Vanilla OS lets one install packages on the host using [`ABRoot`](https://github.com/Vanilla-OS/ABRoot). While not meant for heavy use, this provides flexibility for installing things like kernel modules or drivers. Image-based updates are also planned and may expand upon `ABRoot`'s versatility.

#### Reliance on Flatpak

> Immutable desktops principally rely on Flatpak applications to provide software to users. This can seem too restrictive, especially in the case of software development. For example, the Flatpak version of VSCode seemingly has no easy integration with package management on the Linux desktop.

This is largely fair. One can use a [remote container](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension with a Flatpak application like VSCode, but this may not be the best setup for all use cases.

Flatpak isn't the answer for everything. It struggles with applications which truly need profound system control. For example, one might want to install extra functionality for RBG devices through [OpenRGB](https://flathub.org/apps/details/org.openrgb.OpenRGB). This requires installing udev rules, but Flatpak cannot handle that on its own. One has to do it themselves.

Fortunately, software development tools like VSCode don't need such deep system control. They just need access to programming language packages. While not the easiest under Flatpak, that's certainly doable under immutability. More on this soon.

#### Software availability

Though it's growing every day, Flathub isn't yet the largest software repository on Linux. With the amount of software support the Linux desktop gets in the first place, relying solely on Flathub can be rough sometimes.

Moreover, many applications on Flathub aren't maintained by their official developers. While Flathub is introducing a verfication system for transparency's sake, this problem remains.

#### Solution

[Distrobox](https://github.com/89luca89/distrobox) and, by extension, Vanilla OS's [`apx`](https://documentation.vanillaos.org/docs/apx/), provides some solutions to all aforementioned pain points. It especially helps with the problem of software availability.

### Distrobox

[Distrobox](https://github.com/89luca89/distrobox) lets you have nearly all Linux distributions' software at your fingertips. It's a containerized utility which allows one to use any Linux distribution through the terminal.

![Distrobox](/assets/img/2023/01/distrobox.png){: w="700" h="400" }

Like Flatpak, it's installable on both mutable and immutable distributions. It provides deep system integration and backwards compatibility with outdated software. In addition to great CLI support, it also supports the installation and integration of GUI applications.

For example, one can install the video editing software [LosslessCut](https://mifi.github.io/lossless-cut/) from the Arch User Repository (AUR) while on Ubuntu 18.04. One can also install `gedit` from the Ubuntu repos while on Fedora Silverblue. See [this video](https://www.youtube.com/watch?v=Q2PrISAOtbY&t=448) for what this looks like. It's even possible to use Steam + `mesa-git` from Arch Linux while on Debian stable.

This software availability lets one source the official versions of applications outside Flatpak. Exercise caution, though, as Distrobox does not provide sandboxing like Flatpak does.

The possibilities are nearly endless. Picking a distro for the breadth of its package repositories basically isn't a worthwhile consideration anymore.

#### My setup

Personally, I run Distrobox on Fedora Silverblue so I can benefit from apps found in Ubuntu, Fedora, and Arch Linux.

* I use the coding software [VSCodium](https://vscodium.com/) in a Fedora container so I can easily install and use programming language packages. Here's a [how-to guide](https://github.com/89luca89/distrobox/blob/main/docs/posts/integrate_vscode_distrobox.md#the-easy-one) for this.
* I have an Arch Linux container to use applications from the AUR.
* I have an Ubuntu container which runs [Jekyll](https://jekyllrb.com/) for this website.

Updates aren't a hassle with this system. I have automatic updates [enabled](https://miabbott.github.io/2018/06/13/rpm-ostree-automatic-updates.html) for my base system and for my Flatpaks. I never need to click an update button, and if anything ever goes wrong, I can rollback without worry.

I also just run `distrobox-upgrade --all` in the terminal to upgrade all my distroboxes at once. One can also automate this with `systemd`, but for me, keeping containers up-to-date isn't critical.

While I had to [configure](https://miabbott.github.io/2018/06/13/rpm-ostree-automatic-updates.html) automatic updates on Fedora Silverblue, Vanilla OS fortunately provides them by default.

![Vanilla OS auto-update utility](/assets/img/2023/01/autoupdate.png){: w="700" h="400" }


#### Tinkering

Distrobox has nearly limitless potential for power users. One can [integrate](https://www.youtube.com/watch?v=eM1p47tow4o) systemd in a container, using that compatibility to install integrated virtual machines and other fun stuff. One can also use Distrobox to run another [desktop environment](https://github.com/89luca89/distrobox/blob/main/docs/posts/run_latest_gnome_kde_on_distrobox.md) on top of one's distribution. In short, Distrobox is a huge boon for tinkering. It can work around many of immutability's pain points.

#### `apx`

Vanilla OS aims to simplify Distrobox through [`apx`](https://documentation.vanillaos.org/docs/apx/), a package manager created collaboratively with Distrobox's creator.

`apx` is a stand-in replacement for `apt`; it has a similar syntax.

* To grab packages from Ubuntu, one writes `apx install <package_name>`.
* To grab packages from the AUR, one writes `apx install --aur <package_name>`.
* To install something from Fedora, one writes `apx install --dnf <package_name>`.

No sudo required! Nix and Alpine Linux package support will also likely come in the future. See `apx`'s manpage [here](https://documentation.vanillaos.org/docs/apx/manpage).

This solution is a lot simpler than trying to manage many different distroboxes. I don't always remember what I put in mine.

![My Distroboxes, shown through the GUI app Atoms](/assets/img/2023/01/my_distroboxes.webp){: w="700" h="400" }

I really appreciate that Vanilla OS makes using Distrobox easy. The tool deserves a lot more recognition.

## Vanilla OS pushes desktop Linux forward

In conclusion, we can see a lot of ways Vanilla OS has iterated on and simplified the 'immutable workflow'. Like the immutable distribution [openSUSE MicroOS](https://microos.opensuse.org/), Vanilla OS ships Flathub by default. It also provides Silverblue-esque auto-updating capabilities out of the box.

Additionally, it has quality of life improvements, like good default compatibility with NVIDIA graphics cards. As aforementioned, Vanilla OS also allows flexibility in installing kernel drivers and modules.

Vanilla OS is not perfect in every way.

- I prefer Fedora Silverblue's rollback capabilities.
- I'm also excited about Silverblue's experimental ability to use [container images as an OS](https://www.ypsidanger.com/building-your-own-fedora-silverblue-image/). There's a lot of quality assurance potential there! Unfortunately, Vanilla OS currently doesn't have that capability.
- Vanilla OS also doesn't currently support manual partitioning on the same disk, like [Fedora Silverblue](https://docs.fedoraproject.org/en-US/fedora-silverblue/installation/#known-limitations).

However, Vanilla OS is releasing with an amazing start. They're aware of their distribution's limitations and are actively working on some of them (e.g. exploring image-based updates and adding manual partitioning).

Moreover, I've been using a Vanilla OS-esque immutable workflow for many months now. I really can't go back to traditional Linux desktops. I love having this much software availability and not worrying about updates.

Trust me. It's worth it.

### Install an immutable distribution

You can [download Vanilla OS here](https://vanillaos.org). If you're a fan of GNOME, give it a try! Vanilla OS may introduce other variants in the future (like KDE), so keep an eye out for that.

* You can also check out [Fedora Kinoite](https://kinoite.fedoraproject.org/) for a KDE version of Fedora Silverblue.
* If you'd want another desktop environment or window manager, check out one of the many [custom Silverblue-based desktops](https://github.com/castrojo/awesome-immutable#configs-from-the-community).
* There's also a general list of immutable distros [available here](https://github.com/castrojo/awesome-immutable#distributions).

## Further reading

* [Vanilla OS Handbook](https://handbook.vanillaos.org/)
* [Vanilla OS Documentation](https://documentation.vanillaos.org/)
* [Immutable Linux discord server](https://discord.gg/N4mswFw6ds)
* [Other immutability discussion spaces](https://github.com/castrojo/awesome-immutable#discussion)
* [Distrobox: Try Multiple Linux Distributions via the Terminal](https://itsfoss.com/distrobox/)
* [Setting yourself up for success before trying Fedora Silverblue](https://www.ypsidanger.com/setting-yourself-up-for-success-before-moving-to-fedora-silverblue/)
