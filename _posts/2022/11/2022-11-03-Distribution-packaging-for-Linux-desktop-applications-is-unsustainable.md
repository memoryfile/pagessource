---
title: Distribution packaging for Linux desktop applications is unsustainable
date: 2022-11-03 18:04:00 +/-TTTT
categories: [Linux, Packaging]
tags: [flatpak, Flathub, AUR, Linux gaming, software security, privacy]
---

Traditionally, Linux-based desktop operating systems, also known as *distributions*, provide applications to their users through their respective software repositories. As a user of a distribution, one typically installs apps through the command line. For example, to install [Steam](https://store.steampowered.com/about/), one writes a command such as `sudo apt install steam`.

The software in these repositories all rely on each other to have relatively compatible dependencies to avoid system breakage, and they're typically all packaged by third parties. Oftentimes, these third parties are unpaid volunteers. Moreover, package installation can irreversibly modify one's base system.

This model, which I will refer to as 'the traditional model' in this article, has many issues. The goal of this article is to illuminate these problems and to advocate for universal, [containerized](https://www.redhat.com/en/topics/cloud-native-apps/what-is-containerization), sandboxed application formats like [Flatpak](https://www.flatpak.org/) as a solution.

### Side note

This article will predominantly serve to outline issues with the traditional packaging model. Consequently, there isn't enough room to address all opinions regarding Flatpak itself as an application format. However, I highly encourage everyone to read the article [Response to "Flatpak Is Not the Future"](https://theevilskeleton.gitlab.io/2022/05/16/response-to-flatpak-is-not-the-future.html). It clears up many misconceptions surrounding Flatpak and accurately describes it at a base level. I may write more about Flatpak someday - but the author of the aforementioned article, [Hari Rana](https://theevilskeleton.gitlab.io/blog), has a great body of work on it available right now.

## 'Dependency hell'

Traditional packaging often suffers from a problem informally known as 'dependency hell'. [Dependency hell](https://www.techtarget.com/searchitoperations/definition/dependency-hell) describes how Linux systems rely on specific libraries and versions of software, and if those libraries conflict with each other, installations can fail. 

Conflicts can happen for many reasons. For example, one might install an application and find that it requires a version of dependency which conflicts with another application's dependency requirements. It may even conflict with the base system itself. This leads to failed installations and system instability (in a colloquial sense).

It's worth noting that technical users have the tendency to get by very easily with distribution packages and their dependency conflicts. They spend a lot of time troubleshooting and working around these limitations. When they become experienced, they stop seeing these flaws as actual problems and start [seeing](https://youtu.be/r_6Pg_v6xMg?t=2685) them as something more or less normal. However, non-technical users will often be discouraged from using a Linux distribution when they run into those issues.

Package installation issues and dependency conflicts are also a greater problem in traditional packaging than people emphasize. For instance, between 2010 and 2021, [AskUbuntu](https://askubuntu.com/) received more than [thirty](https://archive.ph/EBrKY) million views on its top ten most popular questions about these issues.

Immutable systems such as [Fedora Silverblue](https://silverblue.fedoraproject.org/) and [Kinoite](https://kinoite.fedoraproject.org/) avoid dependency issues by encouraging Flatpak as a model of decoupling one's base system and applications. Fedora Silverblue also offers base system [rollbacks](https://docs.fedoraproject.org/en-US/fedora-silverblue/updates-upgrades-rollbacks/) on the boot screen, so one can always return to a known good state where dependency issues haven't occurred.

#### Considering `nix` and `guix`

Systems like [`nix`](https://nixos.org/guides/how-nix-works.html) from [NixOS](https://nixos.org/) and `guix` from [GNU Guix](https://guix.gnu.org/) fare better in avoiding dependency issues compared to package managers like `apt` without utilizing containerization. However, they lack some of Flatpak's advantages, such as run-time sandboxing and ease of use for non-technical users (though `nix` may be improving on this front through projects such as [SnowflakeOS](https://snowflakeos.org/) and the [Nix Software Center](https://github.com/vlinkz/nix-software-center)).

Moreover, `nix` and `guix` haven't seen a large adoption rate across the Linux desktop despite their universal nature. This is likely as a result of their requirement for familiarity with Nix expressions and [Lisp](https://en.wikipedia.org/wiki/Lisp_(programming_language)) respectively. For end users, there's a large time investment required to understand these technologies compared to Flatpak.

`apt` and co. remain much more popular - hence, this article largely focuses on them. However, it may be worth learning about [`nix`](https://github.com/nix-community/awesome-nix) and [`guix`](https://boilingsteam.com/i-love-arch-but-gnu-guix-is-my-new-distro/) if their speciality interests you.

### A tale of two Firefoxes

In 2021, Firefox was outdated for months on [Debian](https://www.debian.org/). *The Register* [said](https://archive.ph/nUBho) at the time,

> The problem is that Firefox 91, the current ESR version, includes several dependencies that the current stable version of Debian – 11.1 "Bullseye" – can't fulfill, which poses the maintainers a problem: either update some components of the graphics stack, just for Firefox, or force the Debian version of Firefox to use older dependencies, which is doable but doesn't fix the problem, so it will happen again when the next ESR appears.

In summary, Debian wasn't able to properly update Firefox as a result of dependency conflicts born from its slow release cadence. If Debian used Mozilla's official [Firefox Flatpak](https://flathub.org/apps/details/org.mozilla.firefox), it wouldn't have been an issue, as containerization lets applications use their own dependencies without the worry of rogue libraries creating issues.

Conversely, when Valve discovered that Firefox was [outdated](https://archive.ph/Smc9u) in their SteamOS, they switched to the Flatpak. By [doing](https://archive.ph/w8S6K) so, Valve reduced their maintenance burden and provided a formally supported version of the application.

### Mispackaging and trust

'Dependency hell' often comes about as a result of mispackaging. Third parties are more likely to mispackage a piece of software, as they typically don't know an application's dependencies and build options as well as the application developer themselves.

Additionally, one needs to give an application root access through a package manager during installation. The mispackaging of an application that you install with root can gravely harm the functionality of your system. An application also has access to one's whole system post-installation, including full file system and device access.

#### Steam on Pop!_OS

A prominent example of mispackaging harm is the issue with Steam on [Pop!_OS](https://pop.system76.com/) in the Linus Tech Tips [Linux challenge](https://www.youtube.com/watch?v=0506yDSgU7M). Steam malfunctioning on the distribution occurred as a consequence of [Launchpad](https://wiki.ubuntu.com/Launchpad) not publishing 32-bit packages of a `systemd` update. As a result of dependency conflicts from the situation, Steam was able to remove core components of the X.org display server, making the operating system unusable for those without experience in command line troubleshooting.

![Steam removing X.org](/assets/img/2022/11/pop.jpg){: w="700" h="400" }

With this incident, it's been rightly pointed out that applications simply shouldn't have, by default, the privilege to remove core system components. When they do, issues can cause great harm, and packagers don't have the time and resources necessary to ensure that issues never occur in the first place. As Jorge Castro [says](https://www.ypsidanger.com/lets-just-kill-the-silly-myths/),

> There are over 20,000 source packages in Debian. The idea that you walk into your distro bar and your friendly neighborhood bud/bartender has meticulously curated you a special blend of trusted software and ensuring that upstream isn't being evil is at best a naive way to look at it. Don't believe me? We all go to the same conferences, that person is drowning in unreasonable expectations, critical bugs, unreasonable demands for a volunteer, and all the "glory" involved with being a distribution packager.

Some distribution packagers themselves have remarked similarly. For example, Richard Brown of the openSUSE Project [says](https://archive.ph/3w6Nu),

> I've been packaging RPMs for years. They are NOT a fine, reliable, trustworthy mechanism for delivering software to peoples machines. As a packager, I am REQUIRED to often add scripts that do nasty things on your systems (*as root*) in order to install the software properly. This may be anything and everything from editing configuration files you probably dont want me touching, to tinkering with your databases, to prodding and poking files that really belong to other packages. It's an ugly, nasty, business which can often lead to weird problems depending on which packages people install in which orders, on which filesystems, with which hardware, which services are running etc. 

In summary, it's important to recognize that maintainers are trying their best and that the traditional packaging model itself isn't sustainable. Moreover, containerization and sandboxing prevents critical problems like the Pop!_OS issue in the first place, as applications aren't given unnecessary privileges.

## Delayed updates and security issues

When a third party packages an application, releases will always lag behind the release cadence of the official developer's releases. This makes one's computer insecure in a couple ways:

* One receives security updates less quickly, giving bad actors more of an opportunity to exploit vulnerabilities and compromise one's system.
* One has to trust both the third party packager and the original developer of the application. By having multiple parties in the mix, the chance of one of them sending malware increases. This relates to the concept of [attack surface](https://en.wikipedia.org/wiki/Attack_surface).

Even if one trusts both parties wholeheartedly, there's still an increase of a security risk. A party could get hacked through a software vulnerability and send malware your way. [After all](https://archive.ph/gl7Mk#selection-1769.52-1775.110), "Security vulnerabilities often go undetected for more than four years before being disclosed. Once identified .. a fix [comes] in about four weeks". By using a more slowly-updated web browser package, for example, the window where one can become compromised widens even more.

One may argue that relying on third parties is necessary, as developers shouldn't package their own applications. However, developers are capable of providing builds of their applications for operating systems such as Windows, macOS, iOS, and Android. They're likewise capable of providing a (universal) build for Linux.

### Third parties aren't a shield

Packagers typically aren't auditors. They put trust in app developers, too. It's common for a packager to lack experience with upstream development, but have a lot of experience with downstream packaging.

Moreover, a distribution is only relevant for Linux desktop users when it has a wide range of software available. This means that packagers often package software that they don't use to attract more users and keep their existing users happy. They may often put very little to no effort into testing, as they don't use the application themselves. Thus, they may bump versions of an app without oversight.

### Restricting permissions is essential

There's a lot more vulnerabilities and harmful issues in software than one typically thinks. For instance, in 2015, Steam had a critical bug on Linux [where](https://web.archive.org/web/20220818053746/https://github.com/ValveSoftware/steam-for-linux/issues/3671) if you moved `~/.local/share/steam`, all files owned by the user would be deleted.  More recently, a scan of 4 million Docker images revealed that 51% have [critical flaws](https://securityaffairs.co/wordpress/111833/hacking/docker-hub-scan-analysis.html). Even last year, there was malware found in a npm package with [millions](https://therecord.media/malware-found-in-npm-package-with-millions-of-weekly-downloads/) of weekly downloads.

All these incidents reveal the importance of restricting application permissions in one's system. It's extremely important to mitigate the harm of critical bugs, vulnerabilities, and malware. The idea of restricting permissions came about through the information security principle of [least privilege](https://en.wikipedia.org/wiki/Principle_of_least_privilege).

Just as other operating systems like Android have increased security and privacy through sandboxing, desktop Linux can too. Flatpak sandboxing can deny applications access to files and internet access, among other things. Moreover, Flatpak employs [portals](https://docs.flatpak.org/en/latest/desktop-integration.html#portals), which securely allow applications to access information and [resources](https://flatpak.github.io/xdg-desktop-portal/) (e.g. microphone, camera, location, etc.) outside of a sandbox.

It's important to mention that many apps on the biggest Flatpak app store, [Flathub](https://flathub.org/), aren't sandboxed particularly well. This is possible because Flatpak itself doesn't mandate sandboxing. Moreover, the trend derives from the fact that Flatpak is a relatively new project. Some apps aren't necessarily updated to account for Flatpak's sandboxing capabilities. With time, the situation will likely improve. Flatpak's capabilities are increasingly becoming more well-known as a result of SteamOS and other distributions adopting the format.

Furthermore, Flathub maintainers continually encourage developers submitting new applications to sandbox well and help them do so. On the user side, one can use [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal) to manually restrict the liberal amount of permissions given to apps. One can also view an app's permissions before installation in software stores such as [GNOME Software](https://apps.gnome.org/app/org.gnome.Software/).

## You can't package the world

Free and open source software (FOSS) has experienced a lot of growth recently. GitHub, a home of many FOSS apps, grew by more than [16 million](https://archive.ph/z2dbS) users in 2021 and expects [100 million](https://archive.ph/gl7Mk) users by 2025. However, many of those apps don't reach distribution software repositories because there's simply too much effort required to maintain them all. This reduces the discoverability and usability of FOSS projects as a whole, especially smaller ones.

### Finding and using applications on Linux could be easier

A while back, I was searching for project-oriented productivity tools. Something like [Notion](https://www.notion.so/), but open source. I checked Flathub to see which apps label themselves as 'Notion alternatives' and, unfortunately, came away with nothing. By rummaging through GitHub, however, I eventually discovered [AppFlowy](https://github.com/AppFlowy-IO/AppFlowy) and [Focalboard](https://github.com/mattermost/focalboard). 

At the time, these apps were only officially available as [tarballs](https://www.computerhope.com/jargon/t/tarball.htm). Tarballs generally don't integrate well with Linux desktops, as they lack icons and the ability to be pinned on desktop environments such as GNOME. These applications also weren't available in the software repositories of my distribution ([Fedora](https://getfedora.org/)).

So, while I was happy to use these pieces of software, I experienced inconveniences from these discoverability and usability issues. Nonetheless, I'm certainly not trying to critique the developers of these apps! I'm extremely thankful for their work in the first place, and now AppFlowy is even available on [Flathub](https://flathub.org/apps/details/io.appflowy.AppFlowy). However, perhaps desktop Linux could grow much stronger if it had a universal repository of software by and for the entirety of the Linux desktop ecosystem through Flatpak and, especially, Flathub.

### Flathub's vision

Flathub is aiming for an ecosystem universally available on desktop Linux where applications are maintained by the applications' developers themselves (which means they'd largely just work™). Flathub moreover provides a build system to which developers can connect their automated continuous integration (CI) system, getting fixes and releases out to users faster. [Firefox](https://archive.ph/Ne5ww) and [OBS Studio](https://archive.ph/qXaOj) already take advantage of it. Flathub is also working on integrating a payment system, rendering donations more accessible and consequently encouraging users to donate to FOSS projects.

The infrastructure and community is there, and we can make this universal Linux desktop ecosystem a [reality](https://www.codethink.co.uk/articles/2022/flathub-codethink-collaboration/). Check out [this talk](https://youtu.be/CBPefa0Ckq8?t=3476) to learn more!

### Who's in control?

Flathub is a [community](https://flathub.org/about) project which works collaboratively in places such as [GitHub](https://github.com/flathub) and [Discourse](https://discourse.flathub.org/). Organizations such as [KDE](https://kde.org/) and [GNOME](https://www.gnome.org/), among others, are significantly invested in it, with many apps from those projects available in Flathub.

What's more, adopting Flatpak wouldn't create a locked-in ecosystem. Flatpak's [description](https://flatpak.org/about/) says,

> Flatpak is developed by an independent community, made up of contributors, volunteers and supporting organizations. It is a true upstream open source project, dedicated to providing technology and services that can be used by all, with no vendor lock-in. We have strong links to other Free Software projects, including the Freedesktop project.

This lack of vendor lock-in derives from the fact that Flatpak fundamentally works in a decentralized manner. Anyone can create a Flatpak repository, such as how [ElementaryOS](https://elementary.io/) have done through their [AppCenter](https://appcenter.elementary.io/), and one can enable and use multiple Flatpak repositories at a time. This decentralization results in Flatpak being more robust than relying on one centralized organization to maintain the applications found in one's package manager.

Open source is first class in the Flatpak effort. Manifests (build scripts) are publicly available in the [Flathub organization](https://github.com/flathub/), and users are free to contribute to them, too. Open source applications will always be available to be compiled and put on any Flatpak software repository.

Flathub's vision stands to improve the desktop Linux experience for everyone. By making app installation much more low-maintenance, desktop Linux stands to grow in popularity and consequently receive improved support from stakeholders in the software space. Even if you thrive in the Linux application ecosystem as it is now, you stand to have an even better experience in the future.

## No room for specialized dependencies

Some applications are built in a way that they conflict with common distribution dependencies.

One of the most prominent examples is [OBS Studio](https://obsproject.com/), a recording and streaming application that [patches](https://feaneron.com/2022/02/14/obs-studio-27-2-on-flathub-get-it-while-its-hot/) CEF and FFmpeg internally to enable its browser source and browser docks features. Consequently, before the release of the official OBS Studio Flatpak, many of its builds on Linux had missing or broken functionality by default. This made providing support to Linux users difficult. Only an Ubuntu PPA provided a fully-functioning OBS Studio build.

There's also the divelog application [Subsurface](https://subsurface-divelog.org/). Some Debian maintainers in 2015 [took issue](https://archive.ph/0Jckc) with it packaging `libgit2`, and opted to drop the application as a result. Another example is [Authenticator](https://gitlab.gnome.org/World/Authenticator), since it's relied on a GStreamer GTK4 plugin that [can't](https://archive.ph/P92DR#selection-2691.0-2691.113) be packaged.

Moreover, the offline documentation browser [Zeal](https://zealdocs.org/) encountered [difficulties](https://archive.ph/c6sly) packaging their application for Arch Linux when it updated to Qt 5.10. This is because the application dependended on OpenSSL 1.0 and Qt 5.10 depends on OpenSSL 1.1.

Toolkit versioning can also significantly affect packaging. For instance, [Krita](https://krita.org/) relies on an  older and heavily patched [version](https://archive.ph/sWvrn#selection-11762.0-12984.11) of the GUI toolkit QT. When Krita is built with differing QT versions, it can behave unexpectedly.

Sometimes an application's dependencies are so recent, the most up-to-date Linux distributions don't even have them available in a stable branch yet. [Bottles](https://usebottles.com/) packaged such a dependency with `libadwaita 1.2 alpha` [back](https://archive.ph/XpYXc) in July 2022, enabling more functionality for their app in the process. 

They accomplished this through their Flatpak-first model: by bundling the dependency within the application itself, the developers ensured a stable experience and skipped a multi-year waiting period for older Linux distributions to bring this dependency into the fray. Flatpak's containerization ultimately gave Bottles developers much greater flexibility in their development process.

## Popularity, resources, and slow release cycles

Once a distribution's staff packages its essential components, it often continues onto packaging applications for its software repositories. The extent this takes place depends on how large the distribution's staff is, whether they're paid or unpaid, and more. Monetary support can provide an edge to corporate-supported distributions, as these distributions can invest more money into supporting their software repositories than others.

Moreover, the issue of manpower in distribution packaging incentivizes the packaging and maintenance of highly popular apps first and foremost. This leaves some apps by the wayside, as we saw with AppFlowy and Focalboard in Fedora's software repositories.

A common solution to the maintenance burden of providing satisfactorily large software repositories is releasing new versions of apps less often, as it provides the opportunity to package more apps altogether. For example, [Debian](https://www.debian.org/) only updates apps for non-security reasons after multiple years. This distribution, in a way, attempts to mitigate 'dependency hell' through these slower updates. This solution has influenced the desktop Linux space to frequently favor outdated software in hopes of receiving a reliable experience.

### Relying on Ubuntu's software repositories wasn't the solution

We've also seen various metrics of popularity applied to what flavor of Linux people ought to use themselves. For many years, [Ubuntu](https://ubuntu.com/download/desktop) enjoyed immense popularity, and it was advertised as a default choice in the desktop Linux space. While this influence lingers, many people now use Linux through other flavors, such as Fedora and Arch Linux.

It's fairly clear that settling on Ubuntu was not an ideal solution for the packaging issue. People desire the ability to use different flavors of Linux without compromising functionality. Conversely, settling on Ubuntu software repositories discourages the usage of these flavors. Official vendor support for desktop Linux (where available) is now often only found in the Ubuntu-based Linux sphere. This manifests in many developers only officially offering `.deb` or Snap versions of their applications.

Conversely, Flatpak encourages distribution diversity by being able to run on nearly any distribution. This includes distributions like Alpine Linux and Void Linux. Users of these non-`glibc` distributions can use Flatpak to install apps that depend on `glibc`.

#### The Application Review Board

At one point, Ubuntu tried to scale the traditional application model; efforts such as the [Application Review Board](https://archive.ph/QESCF) and the backports program attempted to bring faster out-of-band updates to popular applications. However, this failed because the pool of existing Debian developers was already too thin. These developers were already busy with the archive as it was, and the attempt to deliver faster updates couldn't scale.

So, while Ubuntu's initiatives were eventually able to speed up application delivery, it was limited to crucial applications like Firefox. The rest of the archive remained static and frozen for the entire life of a release.

### The AUR is not a Flathub alternative

The Arch User Repository (AUR) is a software repository available on Arch Linux where anyone can upload packages and make them available instantly. This model has allowed the repository to house a staggering amount of software, and many people find the AUR invaluable for this reason.

However, the AUR isn't an alternative to Flathub for many reasons:

* AUR packages are only traditionally available in Arch Linux, and not everyone will settle on a single flavor of Linux. Flatpaks accommodate this by being available universally.
* While the AUR hosts some officially maintained packages, it also hosts a great deal of packages maintained by third parties. As aforementioned, sourcing apps from third parties results in security issues. It's more secure to get it from the original source: the developer themselves.
* One is expected to always audit AUR packages before downloading them, as they could contain malware. However, many Arch users don't [audit](https://archive.ph/1gqyD) AUR packages, as most people don't audit the apps they use in general. So, this model frequently presents a security risk.
* The AUR has supply chain attack issues inherent to its model, as third parties are able to upload any packages they want to and push updates without initial auditing. For example, in the past, a bad actor [inserted](https://archive.ph/LxywN) malicious code into a PDF reader and other packages in the AUR by compromising the third-party maintainer's GitHub account.
* AUR packages still suffer from 'dependency hell' issues.
* The AUR doesn't have a place for a donation system.
* The AUR commonly has multiple downloadable versions of the same app, such as [OBS Studio](https://youtu.be/FPjEdrik-eQ). They vary in levels of functionality and, for non-technical users, knowing which one to download can be confusing.
* Packages/applications in the AUR have very limited descriptions of what they are, so they aren't very discoverable by the layman and can't be easily dropped into a traditional-looking app store like GNOME Software.

That being said, it's totally okay to have the AUR around in the Linux world! It just shouldn't be presented as an alternative to having something like Flathub. We need a universal software repository that minimizes security risks, provides discoverability, and doesn't rely on non-technical people auditing packages.

## Flatpak: a more reliable gaming platform

Linux gaming recently experienced a boom with the arrival of Valve's [Steam Deck](https://www.steamdeck.com/). The Steam Deck wholeheartedly embraces Flatpaks, and for good reason. As Linux gaming continues to mature, it stands to benefit from containerized solutions.

### Linux native games

It's common knowledge in the Linux gaming space that Linux native games often degrade and break over time as system dependencies shift. This is one reason why Linux gaming so heavily relies on the [Proton](https://github.com/ValveSoftware/Proton/) translation layer, and why many Linux native games have been discontinued in favor of their Proton counterparts. For the ones that stick around right now, though, there's some obstacles. 

#### Counter-Strike: Global Offensive (CS:GO)

When you try to install CS:GO with a traditionally packaged Steam, you may run into an [issue](https://github.com/ValveSoftware/csgo-osx-linux/issues/2815) where the game shows a black screen upon launch and then crashes. A common solution to this is installing the dependency `libtcmalloc-minimal4` via the software repository of one's distribution, if it's available.

This troubleshooting isn't something a non-technical user wants to deal with, though. Fortunately, Flatpak Steam runs CS:GO just fine, since its runtime (the [Freedesktop](https://freedesktop-sdk.gitlab.io/) runtime) supports everything properly out of the box.

The situation of native Linux native games is unfortunate, as Linux native games can offer better performance than Proton. The increased leveraging of containers can ensure that Linux native games don't break over time. Indeed, Valve has a project called [Pressure Vessel](https://archive.ph/xyq1f) which employs this idea.

#### Native games can't easily bundle their dependencies

Why don't Linux native games bundle their dependencies? `patrakov` on Reddit [says](https://archive.ph/JwpU6),

> I know one game (Don't Starve) that tried to do this and failed spectacularly. Result: on a modern system, their installer only shows square boxes for all text, including English letters. Turns out that they were bundling the GTK2 library, but it was still loading its private modules responsible for rendering the fonts from /usr/lib/gtk-2.0, and they became incompatible.

> The point is that, without Flatpak, bundling dependencies on Linux is so hard that almost nobody can do it correctly.

### `glibc` & Easy Anti-Cheat

Many Linux users recently encountered issues playing games which use [Easy Anti-Cheat](https://www.easy.ac). This is because the popular Linux dependency `glibc` deprecated the dynamic tag `DT_HASH` in its latest release. Easy Anti-Cheat on Linux relies on this tag, so, with its removal, Easy Anti-Cheat games on Linux no longer functioned properly. Specifically, people on rolling release distros like Arch Linux using applications like a traditionally packaged Steam were affected by this issue.

Flatpak Steam (unintentionally) mitigated this issue by carrying an older version of `glibc` in its [runtime](https://archive.ph/nmqBV). This incident shows that the Freedesktop runtime can provide a more stable target for Linux gaming than traditional packaging; standardizing dependencies helps for such a use-case. However, the issue with `glibc` itself is not solved and could affect Flatpak Steam with the next major release of its runtime the following fall season.

### Flatpak in Linux gaming

Valve's new SteamOS has made Flatpak a mature Linux gaming platform. Nearly all gaming applications are usable through Flatpak, and one can install programs like [MangoHud](https://github.com/flightlessmango/MangoHud) and [vkBasalt](https://github.com/DadSchoorse/vkBasalt) through the Flatpak CLI.

However, Proton releases in Steam can use newer Mesa releases than the Freedesktop runtime [supports](https://archive.ph/tKStI). Mesa is a 3D graphics library commonly used for gaming on Linux. The lack of synchronization causes errors in Steam when using the latest releases of Proton, including the default Proton Experiment selection. It's important that major Mesa releases in Proton and the Freedesktop runtime sync up to prevent issues like this. Fortunately, they both release major new versions on a yearly basis, so this is doable.

Workarounds for this issue include manually using older Proton releases or using the [mesa-git](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/wikis/Mesa-git) extension on [flathub-beta](https://discourse.flathub.org/t/how-to-use-flathub-beta/2111), Flathub's repository for beta software builds.

## Developer burden

Without major adoption of a universal application format, Linux app developers often face an expectation to oversee competent support and application packaging for dozens of different Linux distributions. For instance, the gaming app [Lutris](https://lutris.net/downloads) currently advertises support for fourteen different distribution packaging formats. This expectation increases developer burden in many different ways.

### Packaging isn't easy

Developers usually don't have the ability to provide packages for such a variety of distributions, so they must either understand a number of packaging formats or rely on third parties to repackage their software. As aforementioned, third parties are more likely to package the application incorrectly. Remedying issues which stem from these incidents increases developer support burden.

Additionally, understanding different packaging formats themselves can be a learning curve. Complying with distributions' packaging guidelines can also be difficult, because distributions can have widely diverging guidelines on packaging applications.

### Misplaced blame

Developers themselves may also face criticism or [negative attention](https://archive.ph/P92DR) for mispackaged, third party versions of their application. This can lower morale, and dealing with this negativity also requires effort. [Bottles](https://usebottles.com/) developers have addressed this issue in the [open letter](https://usebottles.com/blog/an-open-letter/) *Please don't unofficially ship Bottles in distribution repositories*, saying,

>For more than a year, Bottles has been supplied mainly as a Flatpak, giving us the flexibility and freedom to proceed with development. This, unfortunately, does not translate with distribution packages. As a result, users may get a subpar experience with Bottles when provided unofficially, and may also get a bad impression. Furthermore, this increases open issues in our issue tracker and support channels on chat services we use. This also duplicates our work in the support aspect. Typically, our solution is to ask users to use Flatpak instead, because the switch usually fixes many of those problems, but this has become more and more tiring for us.

### Arduous support scenarios

On top of providing support to dozens of different distributions, developers are also expected to provide support to users who may have significantly altered their system's base via a package manager, and essentially are running unique distributions of their own.

Moreover, a developer must determine if a problem lies in a user's specific system, their distribution, or an unofficial or official version of their application. Many less technical users, assuming they're motivated to raise a bug report in the first place, will not troubleshoot for these variables. They will create a bug report on the issue page of the application itself nonetheless. Therefore, a developer may often spend time redirecting users to distribution packagers, a third party repackager, or whoever is responsible for an issue. 

Furthermore, in the case of packaging issues, a developer's solution to an issue may be rejected or take a significant amount of time to implement. Likewise, a user suffering from a packaging issue may not have the time, ability, or ambition to follow the developer-provided fix. In these cases, the developer also wastes their time. For more details on this issue, see GNOME developer [Tobias Bernard's](https://tobiasbernard.com/) points in [this video](https://youtu.be/OHsGBcnv8oQ).

Lastly, as a result of distributions with slower release cadences, developers may also receive a significant amount of bug reports for issues they already fixed in the latest releases of their application.

### Better development on the Linux desktop

Remedying many of these issues and others through the adoption of a universal app format may noticeably accelerate the development pace of many Linux applications. For example, Bottles developers have [attributed](https://youtu.be/r_6Pg_v6xMg) their rapid 14-day release cycles to their Flatpak-first model. Given that the Linux desktop is heavily dependent on the voluntary development of free and open source software, resolving these development issues would certainly benefit the Linux desktop as a whole.

## What you can do

- Application developers: please consider maintaining a Flatpak version of your app and [submitting](https://github.com/flathub/flathub/wiki/App-Submission) it to Flathub.
- Distribution maintainers: please consider implementing Flathub as a default application source.
- Everyone: If you haven't used Flatpak, consider trying it out as your preferred app installation method! Moreover, if there's an app you use which isn't available on Flathub, try [asking](https://github.com/flathub/flathub/wiki/App-Submission#theres-an-app-that-id-like-to-see-on-flathub-but-im-not-the-developer) the maintainers to submit it. You can also [donate](https://beta.flathub.org/donate) to Flathub and get [involved](https://discourse.flathub.org/) in its community.

## Conclusion

If you've made it this far, thank you so much for taking the time to read the first article on my new blog! I plan to write more about similar topics, so feel free to follow my page via [RSS](https://www.lifewire.com/what-is-an-rss-feed-4684568) or [Mastodon](https://fosstodon.org/@memoryfile). You can also check about my [About Me](https://memoryfile.codeberg.page/about/) for my contact information.

Feel free to [let me know](https://codeberg.org/memoryfile/pagessource/issues) if you've uncovered any factual errors in this article, as well! I truly want it to be an objective, accurate reference on the issues it discusses.

### Thanks to

[Jorge Castro](https://www.ypsidanger.com/) and [Hari Rana](https://theevilskeleton.gitlab.io/) for all their work.

### Further reading

- [Traditional Packaging is not Suitable for Modern Applications](https://theevilskeleton.gitlab.io/2022/08/29/traditional-packaging-is-not-suitable-for-modern-applications.html) by Hari Rana
- [Desktop upgrades don't have to suck!](https://www.ypsidanger.com/desktop-upgrades-dont-have-to-suck/) by Jorge Castro

### Discussion threads

Here's some places where this article has been discussed:

* [On Reddit @ /r/linux](https://www.reddit.com/r/linux/comments/ym6mzy/distribution_packaging_for_linux_desktop/?)
* [On Reddit @ /r/linux_gaming](https://www.reddit.com/r/linux_gaming/comments/ym7cfw/flatpak_a_more_reliable_linux_gaming_platform/)
* [On Twitter](https://twitter.com/castrojo/status/1588603547444133893)
* [On Mastodon](https://qoto.org/@lupyuen/109290811862999219)

### Changelog

* Nov 4, 2022: Updated the article to clarify that the `Freedesktop-sdk` did not intentionally mitigate the `glibc` issue and that the AUR has some officially maintained packages, as well.
* Nov 5, 2022: Clarified that Flatpak itself doesn't mandate sandboxing, mentioned [Kinoite](https://kinoite.fedoraproject.org/), discussed `nix` and `guix`, and updated the Pop!_OS section.
* Dec 30, 2022: Added Zeal example to specialized dependencies issues and improved readability.